" ===
" === 操作系统检测
" ===
let g:iswindows = 0
let g:islinux = 0
if(has("win32") || has("win64") || has("win95") || has("win16"))
    let g:iswindows = 1
else
    let g:islinux = 1
endif


" ===
" === 通用设置
" ===
syntax on
set nocompatible
filetype on
filetype indent on
filetype plugin on
filetype plugin indent on
set number
set encoding=utf-8
set relativenumber

set cursorline
" set cursorcolumn
" highlight CursorLine   cterm=NONE ctermbg=black ctermfg=green guibg=NONE guifg=NONE
" highlight CursorColumn cterm=NONE ctermbg=black ctermfg=green guibg=NONE guifg=NONE
" set listchars=tab:>-,trail:-
set list

set wrap
set showcmd
set wildmenu
"set ambiwidth=double
set t_Co=256

set hlsearch
exec "nohlsearch"
set incsearch
set ignorecase
set smartcase

set showmatch
set noexpandtab
"set shiftwidth=4
"set tabstop=2
"set softtabstop=2
set autoindent
set list


"autocmd FileType go,python,c,java,perl,shell,bash,vim,ruby,cpp set ai
autocmd FileType go,python,c,java,perl,shell,bash,vim,ruby,cpp set sw=2
autocmd FileType go,python,c,java,perl,shell,bash,vim,ruby,cpp set ts=2
autocmd FileType go,python,c,jaa,perl,shell,bash,vim,ruby,cpp set sts=2
"autocmd FileType javascript,html,css,xml set ai
autocmd FileType javascript,html,css,xml set sw=2
autocmd FileType javascript,html,css,xml set ts=2
autocmd FileType javascript,html,css,xml set sts=2


set mouse=a

" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" ===
" === statusline
" ===
set laststatus=2
"set statusline=%1*%F%m%r%h%w%=\ %{&ff}\|\ %2*\ %Y\ %3*%{\"\".(\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\"+\":\"\").\"\"}\ %4*[%l,%v]\ %5*%p%%\ \|\ %6*%LL
"hi User1 cterm=none ctermfg=Black ctermbg=White
"hi User2 cterm=none ctermfg=Black ctermbg=White
"hi User3 cterm=none ctermfg=Black ctermbg=White
"hi User4 cterm=none ctermfg=Black ctermbg=White
"hi User5 cterm=none ctermfg=Black ctermbg=White
"hi User6 cterm=none ctermfg=Black ctermbg=White


" ===
" === 快捷键映射
" ===
map sl :set splitright<CR>:vsplit new<CR>
map sh :set nosplitright<CR>:vsplit new<CR>
map sk :set nosplitbelow<CR>:split new<CR>
map sj :set splitbelow<CR>:split new<CR>

map tn :tabnew<CR>

map gB :bp<CR>
map gb :bn<CR>

map <up> :res +5<CR>
map <down> :res -5<CR>
map <left> :vertical resize-5<CR>
map <right> :vertical resize+5<CR>

map tt :NERDTreeMirror<CR>
map tt :NERDTreeToggle<CR>

nmap <C-s> :w!<CR>
imap <C-s> <esc>:w!<CR>

map <A-S-f> :Prettier<CR>


" ===
" === vim-plug插件目录设置
" ===
if islinux == 1
    call plug#begin('~/.config/nvim/plugged')
endif
if iswindows == 1
    call plug#begin('~\AppData\Local\nvim\plugged')
endif

" Theme && color
Plug 'mhinz/vim-startify'                          " 启动页面

Plug 'scrooloose/nerdtree'                         " 目录树
Plug 'Xuyuanp/nerdtree-git-plugin'                 " 目录树Git插件
Plug 'luochen1990/rainbow'                         " 彩虹括号
Plug 'Yggdroot/indentLine'                         " 可视化缩进
Plug 'mg979/vim-xtabline'                          " 可视化tabline
Plug 'itchyny/lightline.vim'                       " lightline

Plug 'tell-k/vim-autopep8'                         " vim-autopep8,自动格式化

Plug 'tpope/vim-surround'                          " 括号引号成对符号修改插件
Plug 'tpope/vim-fugitive'                          " git

Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' } " go

call plug#end()


" ===
" === 主题颜色
" ===
set background=dark
colorscheme toast
set termguicolors

augroup toast
  autocmd colorscheme toast hi clear Constant | hi link Constant Type
augroup END

highlight Normal guibg=NONE ctermbg=NONE

" ===
" === 插件设置
" ===
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#buffer_nr_show = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" lightline设置
let g:lightline = {
    \ 'colorscheme': 'solarized',
    \ }

" 彩虹括号
let g:rainbow_active = 1

" 可视化缩进
let g:indent_guides_guide_size = 1      " 指定对齐线的尺寸
let g:indent_guides_start_level = 2     " 从第二层开始可视化显示缩进

" startify起始页设置
let g:startify_session_autoload = 1
let g:startify_custom_header = [
            \ '+------------------------------+',
            \ '|                              |',
            \ '|         YukiTechStudio       |',
            \ '|                              |',
            \ '+----------------+-------------+',
            \]

" 自动pep8格式化
autocmd FileType python noremap <buffer> <F6> :call Autopep8()<CR>


" ===
" === coc-settings
" ===
let g:coc_global_extensions = [
\ 'coc-html',
\ 'coc-css',
\ 'coc-tsserver',
\ 'coc-vetur',
\ 'coc-eslint',
\ 'coc-prettier',
\ 'coc-pyright',
\ 'coc-go',
\ 'coc-git',
\ 'coc-json',
\ 'coc-toml',
\ 'coc-vimlsp',
\ 'coc-snippets',
\ 'coc-highlight',
\ 'coc-markdownlint',]

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" prettier settings
command! -nargs=0 Prettier :CocCommand prettier.formatFile

"""""""""""""""""""""""""""""""""""" 
